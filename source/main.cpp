#include "glm/fwd.hpp"
#include "glm/geometric.hpp"
#include "glm/packing.hpp"
#include "glm/trigonometric.hpp"
#include <GL/gl.h>
#include <chrono>
#include <cmath>
#include <glm/glm.hpp>
#include <iostream>
#include <limits>
#include <optional>
#include <ostream>
#include <raylib.h>
#include <string>
#include <tecs.hpp>
#include <thread>
#include <type_traits>

using namespace Tecs;
using namespace std::chrono_literals;

struct Transform2D {
  glm::vec2 position;
  float rotation;
  glm::vec2 scale;
};

struct Sprite {
  Texture texture;
  glm::vec2 size;
};

struct Velocity {
  glm::vec2 value;
};

struct Snowman {
  int snowballs = 10;
  TimePoint last_fired;
};

struct PlayerKeyboardInput {
  KeyboardKey accelerate;
  KeyboardKey left;
  KeyboardKey right;
  KeyboardKey shoot;
};

struct PhysicsSystem : TimedSystem {
  using TimedSystem::TimedSystem;
  void runEntity(const Entity entity) {
    auto &transform = get<Transform2D>(entity);
    auto &[velocity] = get<Velocity>(entity);
    transform.position += velocity * (float)delta.count();
  }
};

struct SpriteRenderingSystem : System {
  using System::System;
  void runEntity(const Entity entity) {
    const auto &sprite = get<Sprite>(entity);
    const auto &[position, rotation, scale] = get<Transform2D>(entity);
    auto scaled = Rectangle{position.x, position.y, sprite.size.x * scale.x,
                            sprite.size.y * scale.y};
    DrawTexturePro(
        sprite.texture, Rectangle{0, 0, sprite.size.x, sprite.size.y}, scaled,
        {scaled.width / 2.0f, scaled.height / 2.0f}, rotation, WHITE);
  }
};

struct CollisionCircle {
  float radius;
};

// With Transform2D.position at the centre
struct CollisionRectangle {
  float width;
  float height;
};

struct CollisionEvent {
  Entity collider;
  // The overlap of the two colliders, directed from "this" to "collider".
  glm::vec2 intersection;
};

enum class ColliderShape {
  Circle,
  Rectangle,
};

struct ColliderInfo {
  ColliderShape shape;
  std::vector<CollisionEvent> collisions;
};

struct Snowball {
  Entity firer;
};

bool circle_point(const glm::vec2 &circle_position, const float circle_radius,
                  const glm::vec2 &point_position) {
  return glm::distance(circle_position, point_position) <= circle_radius;
}

bool rectangle_point(const glm::vec2 &point, const Rectangle &rectangle) {
  return rectangle.x <= point.x && point.x <= rectangle.x + rectangle.width &&
         rectangle.y <= point.y && point.y <= rectangle.y + rectangle.height;
}

/** The closest point in the rectangle to the given point. */
glm::vec2 closest_point_on_rectangle(const glm::vec2 &point,
                                     const Rectangle &rectangle) {
  return {point.x < rectangle.x ? rectangle.x
          : point.x > rectangle.x + rectangle.width
              ? rectangle.x + rectangle.width
              : point.x,
          point.y < rectangle.y ? rectangle.y
          : point.y > rectangle.y + rectangle.height
              ? rectangle.y + rectangle.height
              : point.y};
}

struct CollisionSystem : TimedSystem {
  using TimedSystem::TimedSystem;

  // Detect a circle-circle collision between `a` & `b` and write the result to
  // `info`.
  void circle_circle(const Entity a, const Entity b, ColliderInfo &info) {
    const auto &a_collision_circle = get<CollisionCircle>(a);
    const auto &a_transform = get<Transform2D>(a);

    const auto &b_collision_circle = get<CollisionCircle>(b);
    const auto &b_transform = get<Transform2D>(b);

    if (CheckCollisionCircles({a_transform.position.x, a_transform.position.y},
                              a_collision_circle.radius,
                              {b_transform.position.x, b_transform.position.y},
                              b_collision_circle.radius)) {

      // Determine the "intersection" vector.
      const glm::vec2 centres_displacement =
          b_transform.position - a_transform.position;
      const glm::vec2 intersection =
          glm::normalize(centres_displacement) *
          (a_collision_circle.radius + b_collision_circle.radius -
           glm::length(centres_displacement));

      info.collisions.push_back(CollisionEvent{b, intersection});
    }
  }

  Rectangle make_rectangle(const glm::vec2 &position,
                           const CollisionRectangle &bounds) {
    return Rectangle{position.x - bounds.width / 2.0f,
                     position.y - bounds.height / 2.0f, bounds.width,
                     bounds.height};
  }
  // Collisions between circular a, rectangular b.
  void circle_rectangle(const Entity a, const Entity b, ColliderInfo &info) {
    const auto &a_collision_circle = get<CollisionCircle>(a);
    const auto &a_transform = get<Transform2D>(a);

    const auto &b_collision_rectangle = get<CollisionRectangle>(b);
    const auto &b_transform = get<Transform2D>(b);

    const auto b_rectangle =
        make_rectangle(b_transform.position, b_collision_rectangle);

    const auto closest_point =
        closest_point_on_rectangle(a_transform.position, b_rectangle);

    // Center of circle is inside rectangle; easy to check.
    if (rectangle_point(a_transform.position, b_rectangle)) {

      info.collisions.push_back(
          CollisionEvent{b, b_transform.position - a_transform.position});

    } else if (circle_point(a_transform.position, a_collision_circle.radius,
                            closest_point)) {
      const auto centre_to_edge = closest_point - a_transform.position;
      const auto intersection =
          a_collision_circle.radius * glm::normalize(centre_to_edge) -
          centre_to_edge;
      info.collisions.push_back(CollisionEvent{b, intersection});
    }
  }

  void rectangle_rectangle(const Entity a, const Entity b, ColliderInfo &info) {
    std::ignore = a;
    std::ignore = b;
    std::ignore = info;
    const auto &a_collision_rectangle = get<CollisionRectangle>(a);
    const auto &a_transform = get<Transform2D>(a);

    const auto &b_collision_rectangle = get<CollisionRectangle>(b);
    const auto &b_transform = get<Transform2D>(b);

    Rectangle intersection_rectangle = GetCollisionRec(
        make_rectangle(a_transform.position, a_collision_rectangle),
        make_rectangle(b_transform.position, b_collision_rectangle));
    if (intersection_rectangle.width != 0 ||
        intersection_rectangle.height != 0) {
      info.collisions.push_back(
          CollisionEvent{b, b_transform.position - a_transform.position});
    }
  }

  void run(const std::set<Entity> &entities) {
    for (const auto &a : entities) {

      auto &a_collider_info = get<ColliderInfo>(a);
      a_collider_info.collisions.clear();

      // std::cout << "Collisions for " << a << '\n';
      switch (a_collider_info.shape) {
      case ColliderShape::Circle:
        for (const auto &b : entities) {
          // Entity IDs are ordered, so this prevents repeating the same test
          // multiple times.
          if (b >= a) {
            break;
          }

          switch (get<ColliderInfo>(b).shape) {
          case ColliderShape::Circle:
            circle_circle(a, b, a_collider_info);
            break;
          case ColliderShape::Rectangle:
            circle_rectangle(a, b, a_collider_info);
            break;
          }
        }
        break;

      case ColliderShape::Rectangle:
        for (const auto &b : entities) {
          if (b >= a) {
            break;
          }

          switch (get<ColliderInfo>(b).shape) {
          case ColliderShape::Circle:
            circle_rectangle(b, a, get<ColliderInfo>(b));
            break;
          case ColliderShape::Rectangle:
            rectangle_rectangle(a, b, a_collider_info);
            break;
          }
        }
        break;
      }
    }
  }
};

void make_snowball(Coordinator &ecs, const Entity firer, const Sprite &sprite,
                   const Transform2D &player_transform,
                   const Velocity &player_velocity) {
  glm::vec2 player_facing =
      glm::vec2{std::cos(glm::radians(player_transform.rotation)),
                std::sin(glm::radians(player_transform.rotation))};
  Transform2D snowball_transform = player_transform;
  // Puts the snowball just beyond where it should overlap player.
  snowball_transform.position += 48.0f * player_facing;
  ecs.addComponents(ecs.newEntity(), sprite, snowball_transform,
                    Velocity{player_velocity.value + 300.0f * player_facing},
                    CollisionCircle{16.0f},
                    ColliderInfo{ColliderShape::Circle, {}}, Snowball{firer});
  ecs.getComponent<Velocity>(firer).value -= 150.0f * player_facing;
}

struct PlayerControlSystem : TimedSystem {
  using TimedSystem::TimedSystem;
  Sprite snowball_texture =
      Sprite{LoadTexture("art/snowball.png"), {32.0f, 32.0f}};
  float velocity_drag = 0.995f;
  float acceleration = 300.0f;
  void runEntity(const Entity entity) {
    auto &transform = get<Transform2D>(entity);
    const auto &[accelerate, left, right, shoot] =
        get<PlayerKeyboardInput>(entity);
    auto &velocity = get<Velocity>(entity);

    if (IsKeyDown(left)) {
      transform.rotation -= acceleration * delta.count();
    }
    if (IsKeyDown(right)) {
      transform.rotation += acceleration * delta.count();
    }
    while (transform.rotation < 0) {
      transform.rotation += 360;
    }
    while (transform.rotation > 360) {
      transform.rotation -= 360;
    }

    if (IsKeyDown(accelerate) && glm::length(velocity.value) <= 200.0f) {
      velocity.value += (float)(120.0f * delta.count()) *
                        glm::vec2{std::cos(glm::radians(transform.rotation)),
                                  std::sin(glm::radians(transform.rotation))};
    } else {
      velocity.value *= velocity_drag;
    }
    if (IsKeyPressed(shoot)) {
      auto &snowman = get<Snowman>(entity);
      auto now = TimePoint::clock::now();
      if (snowman.snowballs > 1 && now - snowman.last_fired > 1s) {
        make_snowball(coordinator, entity, snowball_texture, transform,
                      velocity);
        snowman.snowballs -= 1;
        snowman.last_fired = now;
      }
    }
  }
};

struct BouncingCollisionHandlingSystem : System {
  using System::System;
  void snowman_snowball(const Entity snowman_entity,
                        const Entity snowball_entity) {
    auto &snowman = get<Snowman>(snowman_entity);
    snowman.snowballs -= 2;
    if (snowman.snowballs <= 0) {
      coordinator.queueDestroyEntity(snowman_entity);
    }
    coordinator.queueDestroyEntity(snowball_entity);
  }

  void runEntity(const Entity entity) {
    auto &entity_transform = get<Transform2D>(entity);

    const auto &collider_info = get<ColliderInfo>(entity).collisions;
    // TODO: Bounce relative velocities
    auto damped_bounce = [](const auto &velocity, const auto &normal) {
      return glm::reflect(velocity, normal) * 0.8f;
    };

    if (coordinator.hasComponent<Velocity>(entity)) {
      for (const auto &collision : collider_info) {
        const auto intersection_direction =
            glm::normalize(collision.intersection);
        if (coordinator.hasComponent<Velocity>(collision.collider)) {
          if (coordinator.hasComponent<Snowman>(entity) &&
              coordinator.hasComponent<Snowball>(collision.collider)) {
            snowman_snowball(entity, collision.collider);
          } else if (coordinator.hasComponent<Snowball>(entity) &&
                     coordinator.hasComponent<Snowman>(collision.collider)) {
            snowman_snowball(collision.collider, entity);
          }
          // Two entities with velocity.
          const glm::vec2 half_collision = collision.intersection / 2.0f;
          auto &a_velocity = get<Velocity>(entity).value;
          auto &b_velocity = get<Velocity>(collision.collider).value;

          const auto relative_velocity = a_velocity - b_velocity;
          entity_transform.position -= half_collision;
          get<Transform2D>(collision.collider).position += half_collision;

          a_velocity = a_velocity + (damped_bounce(relative_velocity,
                                                   intersection_direction) -
                                     relative_velocity) /
                                        2.0f;
          b_velocity = b_velocity + (damped_bounce(-relative_velocity,
                                                   intersection_direction) +
                                     relative_velocity) /
                                        2.0f;

        } else {
          // One with velocity, one without.
          entity_transform.position -= collision.intersection;
          auto &velocity = get<Velocity>(entity).value;
          velocity = damped_bounce(velocity, intersection_direction);
        }
      }
    } else {
      for (const auto &collision : collider_info) {
        if (coordinator.hasComponent<Velocity>(collision.collider)) {
          get<Transform2D>(collision.collider).position +=
              collision.intersection;
          auto &velocity = get<Velocity>(collision.collider).value;
          velocity = glm::reflect(velocity, collision.intersection);
        } else {
          std::cout << "Two stationary objects should not collide.\n";
          assert(false);
        }
      }
    }
  }
};

struct SnowballCountDisplaySystem : System {
  using System::System;
  void runEntity(const Entity entity) {
    std::string count = std::to_string(get<Snowman>(entity).snowballs);
    const auto &position = get<Transform2D>(entity).position;
    DrawText(count.c_str(), position.x + 32.0f, position.y - 32.0f, 20, BLACK);
  }
};

Duration FRAME_DURATION = 1s / 60.0;

constexpr auto WINDOW_WIDTH = 1280;
constexpr auto WINDOW_HEIGHT = 720;

void build_level(
    Coordinator &ecs, const Texture2D &wall_texture,
    const std::vector<std::tuple<const glm::vec2, const CollisionRectangle>>
        &layout) {

  auto make_wall = [&ecs, &wall_texture](const glm::vec2 &position,
                                         const CollisionRectangle &size) {
    ecs.addComponents(
        ecs.newEntity(), ColliderInfo{ColliderShape::Rectangle, {}}, size,
        Transform2D{position, 0.0, {size.width / 32.0, size.height / 32.0}},
        Sprite{wall_texture, {32, 32}});
  };
  // Outer borders
  make_wall({16, GetScreenHeight() / 2.0},
            {32.0, static_cast<float>(GetScreenHeight()) - 64.0f});
  make_wall({GetScreenWidth() - 16.0, GetScreenHeight() / 2.0},
            {32.0, static_cast<float>(GetScreenHeight()) - 64.0f});
  make_wall({GetScreenWidth() / 2.0, 16.0},
            {static_cast<float>(GetScreenWidth()) - 64.0f, 32.0});
  make_wall({GetScreenWidth() / 2.0, GetScreenHeight() - 16.0},
            {static_cast<float>(GetScreenWidth()) - 64.0f, 32.0});

  for (const auto &wall : layout) {
    make_wall(std::get<0>(wall), std::get<1>(wall));
  }
}

std::vector<std::vector<std::tuple<const glm::vec2, const CollisionRectangle>>>
    levels = {
        {},
        {
            {{WINDOW_WIDTH / 4.0, WINDOW_HEIGHT / 2.0}, {32.0, 256.0}},
            {{WINDOW_WIDTH / 2.0, WINDOW_HEIGHT / 2.0}, {256, 32.0}},
            {{3.0 * WINDOW_WIDTH / 4.0, WINDOW_HEIGHT / 2.0}, {32.0, 256.0}},
        },
        {
            {{WINDOW_WIDTH / 4.0, WINDOW_HEIGHT / 2.0}, {256, 32.0}},
            {{WINDOW_WIDTH / 2.0, WINDOW_HEIGHT / 2.0}, {32.0, 256.0}},
            {{3.0 * WINDOW_WIDTH / 4.0, WINDOW_HEIGHT / 2.0}, {256, 32.0}},
        },
        {
            {{WINDOW_WIDTH / 2.0, WINDOW_HEIGHT / 2.0}, {700.0, 32.0}},
            {{WINDOW_WIDTH / 2.0, (WINDOW_HEIGHT / 2.0) - 80 - 16}, {32, 160}},
            {{WINDOW_WIDTH / 2.0, (WINDOW_HEIGHT / 2.0) + 80 + 16}, {32, 160}},
        }};

int main(int argc, char *argv[]) {

  long level;
  if (argc > 1) {
    level = std::stol(argv[1]);
    if (level < 0 or (unsigned long) level >= levels.size()) {
      std::cout << "Invalid level number.\n"
                << "Must be from 0 to " << levels.size() - 1 << '\n';
      level = 1;
    }
  } else {
    level = 1;
  }

  InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Snowmen go Sledding");

  Coordinator ecs;
  ecs.registerComponent<Transform2D>();
  ecs.registerComponent<Sprite>();
  ecs.registerComponent<Velocity>();
  ecs.registerComponent<PlayerKeyboardInput>();
  ecs.registerComponent<ColliderInfo>();
  ecs.registerComponent<CollisionCircle>();
  ecs.registerComponent<CollisionRectangle>();
  ecs.registerComponent<Snowman>();
  ecs.registerComponent<Snowball>();

  SpriteRenderingSystem sprite_rendering_system(
      {ecs.componentMask<Transform2D, Sprite>()}, ecs);
  PhysicsSystem physics_system({ecs.componentMask<Transform2D, Velocity>()},
                               ecs);
  PlayerControlSystem player_control_system(
      {ecs.componentMask<Transform2D, Velocity, PlayerKeyboardInput>()}, ecs);
  CollisionSystem collision_system(
      {ecs.componentMask<Transform2D, ColliderInfo>()}, ecs);
  BouncingCollisionHandlingSystem bouncing_collision_handling_system(
      {ecs.componentMask<Transform2D, ColliderInfo>()}, ecs);

  SnowballCountDisplaySystem snowball_count_display_system(
      {ecs.componentMask<Snowman>()}, ecs);

  auto ice_block_texture = LoadTexture("art/ice-block.png");
  build_level(ecs, ice_block_texture, levels[level]);

  {
    auto make_player = [&ecs](const Sprite &sprite, const glm::vec2 &position,
                              const PlayerKeyboardInput &keyboard_input,
                              float rotation) {
      const auto id = ecs.newEntity();
      std::cout << "Player with ID: " << id << std::endl;
      ecs.addComponents(id, Transform2D{position, rotation, {1.0, 1.0}}, sprite,
                        Velocity{}, keyboard_input, CollisionCircle{32},
                        ColliderInfo{ColliderShape::Circle, {}},
                        Snowman{10, TimePoint::clock::now() - 1s});
    };

    Sprite snowman_sprite = Sprite{LoadTexture("art/snowman.png"), {64, 64}};
    make_player(snowman_sprite, {100, GetScreenHeight() / 2.0f},
                {KEY_W, KEY_A, KEY_D, KEY_SPACE}, 0.0f);
    snowman_sprite.texture = LoadTexture("art/red-snowman.png");
    make_player(snowman_sprite,
                {GetScreenWidth() - 100.0f, GetScreenHeight() / 2.0f},
                {KEY_UP, KEY_LEFT, KEY_RIGHT, KEY_PERIOD}, 180.0f);
  }

  auto previous_tick = TimePoint::clock::now() - FRAME_DURATION;

  while (!WindowShouldClose()) {
    auto tick = TimePoint::clock::now();
    auto delta = tick - previous_tick;

    player_control_system.coordinatedRun(delta);
    physics_system.coordinatedRun(delta);
    collision_system.coordinatedRun(delta);
    bouncing_collision_handling_system.coordinatedRun();
    ecs.destroyQueued();

    ClearBackground(WHITE);
    BeginDrawing();
    sprite_rendering_system.coordinatedRun();
    snowball_count_display_system.coordinatedRun();
    EndDrawing();

    previous_tick = tick;
  }

  CloseWindow();
}
